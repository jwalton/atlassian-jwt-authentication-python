#!/usr/bin/python3

# Verify a JWT

from atlassian_jwt_authentication.resourceserver import JwtValidator
import requests
from cachecontrol import CacheControl
from cachecontrol.caches import FileCache
from cachecontrol.heuristics import OneDayCache

base = 'https://s3-ap-southeast-2.amazonaws.com/keymaker.syd.dev.atlassian.io'

sess = CacheControl(requests.Session(), cache = FileCache('.cache'), heuristic = OneDayCache())

validator = JwtValidator(base, sess)

jws = 'eyJraWQiOiAiandhbHRvbi9zYW1wbGUtaW4tanNvbi13ZWItc2lnbmF0dXJlLWRyYWZ0IiwgImFsZyI6ICJSUzI1NiJ9.eyJpYXQiOiAxNDIzMTIxNzI2LCAic3ViIjogImp3YWx0b24iLCAiZXhwIjogMTQyMzEyNTMyNiwgImlzcyI6ICJqd2FsdG9uIiwgImF1ZCI6ICJoaXBjaGF0L1MyUyBBdXRoIn0.MtS6ol3of-5ctt0nA1klJ6FrOFvMHgsiI5yRSrstbj1tCmaGjE661-_XkkgOwYtUpYVyXTCFAHmKpsADmZZEOTGr2xY74s4gbQFxGSnFm8t0NJ0HQAyrvkGmjgX-RqcNXmGtMzspuDCdT8nLPsmwpmsKN1czZ9BmyfEKesnuhhR4R7rMkFKgPJ5d1eqvdeDkLU4rd9J0yaIqtUOlpMX479sUABRZFSDtjLFdrvkPGnQmDfTYZVnJCk2UJdC1Rg2K0xuWebzDeRmSJkKy3hrpUryN2e3BbQ_eME02DjrkcPwnF0HAe5KDHlgaQODiUzMlZXs2_J8OCe_niUn62XTSIQ'

claims = validator.valid_claims(jws, 'hipchat/S2S Auth')
print('Audience: ' + claims['aud'])
