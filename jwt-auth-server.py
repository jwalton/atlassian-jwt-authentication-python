#!/usr/bin/python3

from wsgiref.simple_server import make_server

from atlassian_jwt_authentication.resourceserver import JwtValidator
import requests
from cachecontrol import CacheControl
from cachecontrol.caches import FileCache
from cachecontrol.heuristics import OneDayCache

base = 'https://s3-ap-southeast-2.amazonaws.com/keymaker.syd.dev.atlassian.io'
sess = CacheControl(requests.Session(), cache = FileCache('.cache'), heuristic = OneDayCache())
validator = JwtValidator(base, sess)

CTH = ('Content-Type', 'text/plain; charset=utf-8')
def body(s):
    return [s.encode('utf-8')]

def simple_app(environ, start_response):
    if 'HTTP_AUTHORIZATION' not in environ:
        start_response('401 Unauthorized', [CTH, ('WWW-Authenticate', 'Bearer')])
        return body('Unauthorized.\n')

    auth = environ['HTTP_AUTHORIZATION']

    (authScheme, *params) = auth.split()

    if authScheme.lower() != 'bearer':
        start_response('401 Unauthorized', [CTH, ('WWW-Authenticate', 'Bearer')])
        return body('Unknown authentication scheme.\n')

    if len(params) != 1:
        start_response('401 Unauthorized', [CTH, ('WWW-Authenticate', 'Bearer')])
        return body('Unexpected params for bearer authorization.\n')

    claims = validator.valid_claims(params[0], 'python-jwt-auth-server')

    start_response('200 Okay', [CTH])

    return body("This is a request from '%s' to '%s'.\n" % (claims['sub'], claims['aud']))

PORT = 8000

httpd = make_server('', PORT, simple_app)
print('Listening on port %d' % PORT)
httpd.serve_forever()
