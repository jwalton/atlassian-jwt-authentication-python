#!/usr/bin/python3

import unittest
from atlassian_jwt_authentication.client import JwtSigner
from Crypto.PublicKey import RSA

class TestJwk(unittest.TestCase):
    def test_claims_times_are_in_expected_order(self):
        k = RSA.generate(1024)
        signer = JwtSigner('test', 'test/test', k)
        claims = signer.make_claims('python-jwt-auth-server')

        self.assertGreaterEqual(claims['exp'], claims['iat'], msg='Expiry must be at least issue time')

    def test_claims_include_jwt_id(self):
        k = RSA.generate(1024)
        signer = JwtSigner('test', 'test/test', k)
        claims = signer.make_claims('python-jwt-auth-server')

        self.assertIsNotNone(claims['jti'])
        self.assertIsInstance(claims['jti'], str)

    def test_jwt_id_is_not_trivially_recycled(self):
        k = RSA.generate(1024)
        signer = JwtSigner('test', 'test/test', k)

        claims1 = signer.make_claims('python-jwt-auth-server')
        claims2 = signer.make_claims('python-jwt-auth-server')

        self.assertNotEqual(claims1['jti'], claims2['jti'])


if __name__ == '__main__':
    unittest.main()
