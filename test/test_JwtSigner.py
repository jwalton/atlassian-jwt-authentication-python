#!/usr/bin/python3

import unittest
import array

import atlassian_jwt_authentication


class TestJwtSigner(unittest.TestCase):
  # https://tools.ietf.org/html/draft-ietf-jose-json-web-signature-41#appendix-C
  def test_base64Url_encoding(self):
    self.assertEqual(atlassian_jwt_authentication.base64Url(array.array('B', [3, 236, 255, 224, 193]).tobytes()), 'A-z_4ME')

  def test_base64Url_decoding(self):
    self.assertEqual(bytes([3, 236, 255, 224, 193]), atlassian_jwt_authentication.decodeBase64Url('A-z_4ME'))

  # https://tools.ietf.org/html/draft-ietf-jose-json-web-signature-41#section-3.3
  def test_encode_protected_header(self):
    header = '{"typ":"JWT",\r\n "alg":"HS256"}'
    self.assertEqual(atlassian_jwt_authentication.base64Url(atlassian_jwt_authentication.utf8(header)), 'eyJ0eXAiOiJKV1QiLA0KICJhbGciOiJIUzI1NiJ9')

  # https://tools.ietf.org/html/draft-ietf-jose-json-web-signature-41#section-3.3
  def test_encode_jws_payload(self):
    payload = '{"iss":"joe",\r\n "exp":1300819380,\r\n "http://example.com/is_root":true}'
    self.assertEqual(atlassian_jwt_authentication.base64Url(atlassian_jwt_authentication.utf8(payload)),
      'eyJpc3MiOiJqb2UiLA0KICJleHAiOjEzMDA4MTkzODAsDQogImh0dHA6Ly9leGFtcGxlLmNvbS9pc19yb290Ijp0cnVlfQ')

  # https://tools.ietf.org/html/draft-ietf-jose-json-web-signature-41#appendix-A.1
  def test_sign_and_encode_with_HMAC_SHA_256(self):
    jwsProtectedHeader = '{"typ":"JWT",\r\n "alg":"HS256"}'
    jwsPayload = atlassian_jwt_authentication.utf8('{"iss":"joe",\r\n "exp":1300819380,\r\n "http://example.com/is_root":true}')
    key = atlassian_jwt_authentication.decodeBase64Url('AyM1SysPpbyDfgZld3umj1qzKObwVMkoqQ-EstJQLr_T-1qS0gZH75aKtMN3Yj0iPS4hcgUuTwjAzZr1Z9CAow')

    signingInputValue = atlassian_jwt_authentication.signing_input(jwsProtectedHeader, jwsPayload)
    self.assertEqual(list(bytes(signingInputValue, 'us-ascii')), [101, 121, 74, 48, 101, 88, 65, 105, 79, 105, 74, 75, 86, 49, 81,
   105, 76, 65, 48, 75, 73, 67, 74, 104, 98, 71, 99, 105, 79, 105, 74,
   73, 85, 122, 73, 49, 78, 105, 74, 57, 46, 101, 121, 74, 112, 99, 51,
   77, 105, 79, 105, 74, 113, 98, 50, 85, 105, 76, 65, 48, 75, 73, 67,
   74, 108, 101, 72, 65, 105, 79, 106, 69, 122, 77, 68, 65, 52, 77, 84,
   107, 122, 79, 68, 65, 115, 68, 81, 111, 103, 73, 109, 104, 48, 100,
   72, 65, 54, 76, 121, 57, 108, 101, 71, 70, 116, 99, 71, 120, 108, 76,
   109, 78, 118, 98, 83, 57, 112, 99, 49, 57, 121, 98, 50, 57, 48, 73,
   106, 112, 48, 99, 110, 86, 108, 102, 81])

    signature = atlassian_jwt_authentication.sign_hmac_sha_256(key, atlassian_jwt_authentication.utf8(signingInputValue))
    self.assertEqual(signature,
      array.array('B', [116, 24, 223, 180, 151, 153, 224, 37, 79, 250, 96, 125, 216, 173,
        187, 186, 22, 212, 37, 77, 105, 214, 191, 240, 91, 88, 5, 88, 83,
        132, 141, 121]).tobytes())

    self.assertEqual(atlassian_jwt_authentication.base64Url(signature), 'dBjftJeZ4CVP-mB92K27uhbUJU1p1r_wW1gFWFOEjXk')

    jws = atlassian_jwt_authentication.append_signature(signingInputValue, signature)
    self.assertEqual(jws, 'eyJ0eXAiOiJKV1QiLA0KICJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJqb2UiLA0KICJleHAiOjEzMDA4MTkzODAsDQogImh0dHA6Ly9leGFtcGxlLmNvbS9pc19yb290Ijp0cnVlfQ.dBjftJeZ4CVP-mB92K27uhbUJU1p1r_wW1gFWFOEjXk')

  def test_decodeBase64UrlUint(self):
    self.assertEqual(atlassian_jwt_authentication.decodeBase64UrlUInt('AA'), 0)

  # https://tools.ietf.org/html/draft-ietf-jose-json-web-signature-41#appendix-A.2.1
  def test_sign_and_encode_with_RSASSA_PKCS_v1_5_SHA_256(self):
    jwsProtectedHeader = '{"alg":"RS256"}'
    jwsPayload = atlassian_jwt_authentication.utf8('{"iss":"joe",\r\n "exp":1300819380,\r\n "http://example.com/is_root":true}')

    signingInputValue = atlassian_jwt_authentication.signing_input(jwsProtectedHeader, jwsPayload)

    self.assertEqual(list(bytes(signingInputValue, 'us-ascii')),
                     [101, 121, 74, 104, 98, 71, 99, 105, 79, 105, 74, 83, 85, 122, 73,
                      49, 78, 105, 74, 57, 46, 101, 121, 74, 112, 99, 51, 77, 105, 79, 105,
                      74, 113, 98, 50, 85, 105, 76, 65, 48, 75, 73, 67, 74, 108, 101, 72,
                      65, 105, 79, 106, 69, 122, 77, 68, 65, 52, 77, 84, 107, 122, 79, 68,
                      65, 115, 68, 81, 111, 103, 73, 109, 104, 48, 100, 72, 65, 54, 76,
                      121, 57, 108, 101, 71, 70, 116, 99, 71, 120, 108, 76, 109, 78, 118,
                      98, 83, 57, 112, 99, 49, 57, 121, 98, 50, 57, 48, 73, 106, 112, 48,
                      99, 110, 86, 108, 102, 81])


    n = atlassian_jwt_authentication.decodeBase64UrlUInt('ofgWCuLjybRlzo0tZWJjNiuSfb4p4fAkd_wWJcyQoTbji9k0l8W26mPddxHmfHQp-Vaw-4qPCJrcS2mJPMEzP1Pt0Bm4d4QlL-yRT-SFd2lZS-pCgNMsD1W_YpRPEwOWvG6b32690r2jZ47soMZo9wGzjb_7OMg0LOL-bSf63kpaSHSXndS5z5rexMdbBYUsLA9e-KXBdQOS-UTo7WTBEMa2R2CapHg665xsmtdVMTBQY4uDZlxvb3qCo5ZwKh9kG4LT6_I5IhlJH7aGhyxXFvUK-DWNmoudF8NAco9_h9iaGNj8q2ethFkMLs91kzk2PAcDTW9gb54h4FRWyuXpoQ')
    e = atlassian_jwt_authentication.decodeBase64UrlUInt('AQAB')
    d = atlassian_jwt_authentication.decodeBase64UrlUInt('Eq5xpGnNCivDflJsRQBXHx1hdR1k6Ulwe2JZD50LpXyWPEAeP88vLNO97IjlA7_GQ5sLKMgvfTeXZx9SE-7YwVol2NXOoAJe46sui395IW_GO-pWJ1O0BkTGoVEn2bKVRUCgu-GjBVaYLU6f3l9kJfFNS3E0QbVdxzubSu3Mkqzjkn439X0M_V51gfpRLI9JYanrC4D4qAdGcopV_0ZHHzQlBjudU2QvXt4ehNYTCBr6XCLQUShb1juUO1ZdiYoFaFQT5Tw8bGUl_x_jTj3ccPDVZFD9pIuhLhBOneufuBiB4cS98l2SR_RQyGWSeWjnczT0QU91p1DhOVRuOopznQ')

    from Crypto.PublicKey import RSA

    k = RSA.construct((n, e, d))

    from Crypto.Signature import PKCS1_v1_5
    from Crypto.Hash import HMAC, SHA256
    p = PKCS1_v1_5.new(k)

    h = SHA256.new(atlassian_jwt_authentication.utf8(signingInputValue))
    signature = p.sign(h)

    self.assertEqual(signature,
                     array.array('B', [112, 46, 33, 137, 67, 232, 143, 209, 30, 181, 216, 45, 191, 120, 69,
                                       243, 65, 6, 174, 27, 129, 255, 247, 115, 17, 22, 173, 209, 113, 125,
                                       131, 101, 109, 66, 10, 253, 60, 150, 238, 221, 115, 162, 102, 62, 81,
                                       102, 104, 123, 0, 11, 135, 34, 110, 1, 135, 237, 16, 115, 249, 69,
                                       229, 130, 173, 252, 239, 22, 216, 90, 121, 142, 232, 198, 109, 219,
                                       61, 184, 151, 91, 23, 208, 148, 2, 190, 237, 213, 217, 217, 112, 7,
                                       16, 141, 178, 129, 96, 213, 248, 4, 12, 167, 68, 87, 98, 184, 31,
                                       190, 127, 249, 217, 46, 10, 231, 111, 36, 242, 91, 51, 187, 230, 244,
                                       74, 230, 30, 177, 4, 10, 203, 32, 4, 77, 62, 249, 18, 142, 212, 1,
                                       48, 121, 91, 212, 189, 59, 65, 238, 202, 208, 102, 171, 101, 25, 129,
                                       253, 228, 141, 247, 127, 55, 45, 195, 139, 159, 175, 221, 59, 239,
                                       177, 139, 93, 163, 204, 60, 46, 176, 47, 158, 58, 65, 214, 18, 202,
                                       173, 21, 145, 18, 115, 160, 95, 35, 185, 232, 56, 250, 175, 132, 157,
                                       105, 132, 41, 239, 90, 30, 136, 121, 130, 54, 195, 212, 14, 96, 69,
                                       34, 165, 68, 200, 242, 122, 122, 45, 184, 6, 99, 209, 108, 247, 202,
                                       234, 86, 222, 64, 92, 178, 33, 90, 69, 178, 194, 85, 102, 181, 90,
                                       193, 167, 72, 160, 112, 223, 200, 163, 42, 70, 149, 67, 208, 25, 238,
                                       251, 71]).tobytes())

    jws = atlassian_jwt_authentication.append_signature(signingInputValue, signature)
    self.assertEqual(jws, '''
     eyJhbGciOiJSUzI1NiJ9
     .
     eyJpc3MiOiJqb2UiLA0KICJleHAiOjEzMDA4MTkzODAsDQogImh0dHA6Ly9leGFt
     cGxlLmNvbS9pc19yb290Ijp0cnVlfQ
     .
     cC4hiUPoj9Eetdgtv3hF80EGrhuB__dzERat0XF9g2VtQgr9PJbu3XOiZj5RZmh7
     AAuHIm4Bh-0Qc_lF5YKt_O8W2Fp5jujGbds9uJdbF9CUAr7t1dnZcAcQjbKBYNX4
     BAynRFdiuB--f_nZLgrnbyTyWzO75vRK5h6xBArLIARNPvkSjtQBMHlb1L07Qe7K
     0GarZRmB_eSN9383LcOLn6_dO--xi12jzDwusC-eOkHWEsqtFZESc6BfI7noOPqv
     hJ1phCnvWh6IeYI2w9QOYEUipUTI8np6LbgGY9Fs98rqVt5AXLIhWkWywlVmtVrB
     p0igcN_IoypGlUPQGe77Rw'''.replace('\n', '').replace(' ', ''))

if __name__ == '__main__':
  unittest.main()
