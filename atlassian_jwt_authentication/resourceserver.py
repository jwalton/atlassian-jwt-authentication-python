import json
import atlassian_jwt_authentication

class JwtValidator:
    def __init__(self, baseUrl, requestsSession):
        self.baseUrl = baseUrl
        self.requestsSession = requestsSession
        self.seen_jti = {}

    # XXX This does *not* validate the claims that are returned
    def valid_claims(self, jws, aud):
        (hdr_b64, claims_b64, signature_b64) = jws.split('.')

        signingInputValue = hdr_b64 + '.' + claims_b64

        hdr = json.loads(atlassian_jwt_authentication.decodeBase64Url(hdr_b64).decode('utf-8'))

        # XXX Confirm algorithm, ensure kid is a slash-separated pair of URL path segments
        print('Algorithm: ' + hdr['alg'])
        print('Key ID: ' + hdr['kid'])

        claims = json.loads(atlassian_jwt_authentication.decodeBase64Url(claims_b64).decode('utf-8'))

        # XXX Ensure 'iss' and 'sub' match, and are one or more slash-separated URL path segments
        print('Issuer: ' + claims['iss'])

        # XXX Ensure 'kid' starts with 'iss' /

        signature = atlassian_jwt_authentication.decodeBase64Url(signature_b64)

        keyUrl = self.baseUrl + '/' + hdr['kid']

        print('Key URL: ' + keyUrl)

        PEM_FILE_TYPE='application/x-pem-file'

        keyResp = self.requestsSession.get(keyUrl, headers={'Accept': PEM_FILE_TYPE})
        if keyResp.status_code != 200:
            raise Exception('Unable to retrieve key')

        if keyResp.headers['Content-Type'] != PEM_FILE_TYPE:
            raise Exception('Unexpected content-type for key: %s' % keyResp.headers['Content-Type'])

        w = keyResp.headers['warning']
        if w is not None:
            print('Warning: ' + w)

        from Crypto.PublicKey import RSA

        k = RSA.importKey(keyResp.text)
        print(k.exportKey().decode('us-ascii'))

        from Crypto.Signature import PKCS1_v1_5
        from Crypto.Hash import HMAC, SHA256
        p = PKCS1_v1_5.new(k)

        h = SHA256.new(atlassian_jwt_authentication.utf8(signingInputValue))

        print('Verified: %d' % p.verify(h, signature))

        jti = claims['jti']

        if jti in self.seen_jti:
            raise Exception('Repeated JWT ID: %s' % jti)

        self.seen_jti[jti] = 1

        print('JWT ID: %s' % jti)

        import time
        now = time.time()

        if claims['iat'] > now:
            print('Has not been issued yet.')
        elif claims['exp'] <= now:
            print('Has expired.')
        else:
            print('Still valid')

        return claims
