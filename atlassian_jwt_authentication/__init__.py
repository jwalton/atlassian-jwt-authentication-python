import base64

# draft-ietf-jose-json-web-signature-41#appendix-C
def base64Url(b):
  u = str(base64.b64encode(b), 'us-ascii')

  return u.split('=')[0].replace('+', '-').replace('/', '_')

def decodeBase64Url(s):
  bs = s.replace('_', '/').replace('-', '+')
  if len(s) % 4 == 2:
    bs = bs + '=='
  elif len(s) % 4 == 3:
    bs = bs + '='

  return base64.b64decode(bs)

def decodeBase64UrlUInt(s):
  b = decodeBase64Url(s)
  i = 0
  for x in b:
    i = i * 0x100
    i = i + int(x)

  return i

def signing_input(jwsProtectedHeader, jwsPayload):
  return base64Url(utf8(jwsProtectedHeader)) + '.' + base64Url(jwsPayload)

def sign_hmac_sha_256(k, m):
  import hmac
  import hashlib

  return hmac.new(k, msg=m, digestmod=hashlib.sha256).digest()

def append_signature(signing_input, signature):
  return signing_input + '.' + base64Url(signature)

def utf8(s):
  return bytes(s, 'utf-8')
