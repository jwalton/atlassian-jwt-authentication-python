import json
import time

import atlassian_jwt_authentication

from Crypto.Signature import PKCS1_v1_5
from Crypto.Hash import HMAC, SHA256

import random

class JwtSigner:
    def __init__(self, iss, kid, key, lifetime=3600):
        self.iss = iss
        self.key = key
        self.lifetime = lifetime
        self.hdr = {'alg': 'RS256', 'kid': kid}

        self.random = random.Random()

    def make_claims(self, aud):
        now = time.time()

        claims = {'iss': self.iss,
         'sub': self.iss,
         'iat': now,
         'exp': now + self.lifetime,
         'aud': aud,
         'jti': "%d:%d" % (now, self.random.getrandbits(64))}

        return claims

    def sign_request(self, claims):
        signingInputValue = atlassian_jwt_authentication.signing_input(json.dumps(self.hdr), atlassian_jwt_authentication.utf8(json.dumps(claims)))

        p = PKCS1_v1_5.new(self.key)

        h = SHA256.new(atlassian_jwt_authentication.utf8(signingInputValue))
        signature = p.sign(h)

        return atlassian_jwt_authentication.append_signature(signingInputValue, signature)
