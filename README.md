## Atlassian JWT authentication, in Python

An implementation of [Service to Service Authentication](https://extranet.atlassian.com/display/I/Service+to+Service+Authentication+-+Specification),
in Python.

### Install

    pip install pycrypto requests cachecontrol lockfile

(Use 'pip' in a venv, or 'pip install --user'.)

### Generate a token

    python3 make-request.py

### Validate a token

    python3 validate_token.py

### Run a server

    python3 jwt-auth-server.py

### Send signed requests

    python3 jwt-auth-client

Both client and server assume that keys are in place; you'll need to generate and install them, as well
as fixing the hardcoded paths.

This is intended to interoperate with the working version of the spec. Most things are hardcoded and there's no
usable API.

## Generating keys

## Generate a key pair

    openssl genrsa 2048 >private.key

## Extract the public key to publish

    openssl rsa -in private.key -pubout >public-key.pem
