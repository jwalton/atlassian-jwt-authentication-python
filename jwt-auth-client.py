#!/usr/bin/python3

from atlassian_jwt_authentication.client import JwtSigner
import requests
from Crypto.PublicKey import RSA

target = 'http://localhost:8000/a-specific-local-service'
aud = 'python-jwt-auth-server'


k = RSA.importKey(open('sample-in-json-web-signature-draft.private').read())

signer = JwtSigner('jwalton', 'jwalton/sample-in-json-web-signature-draft', k)

claims = signer.make_claims('python-jwt-auth-server')

jws = signer.sign_request(claims)

print('Sending bearer token: %s' % jws)

sess = requests.Session()

resp = sess.get(target, headers={'Authorization': 'Bearer ' + jws})

print('Status code: %d' % resp.status_code)
print('Headers: %s' % resp.headers)
print()
print(resp.text)
